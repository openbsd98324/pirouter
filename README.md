# pirouter

## 1. Introduction and easy installation

Transform your Raspberry PI into a wifi "router", using eth0 ==> wlan0 (bridge).

The gateway will be the default router (main router, like openwrt on ac1200 netgear...)


` sh install.sh `


 


## 2. Installation 

install pirouter (see content of this repository)

On raspbian, "sudo su" and 
` sh install.sh `





## 3. Net Access Control for PCs or Tablets

Turn on or off, the forward ("access") to net.



**1. Remove access to Net (like Roblox) and it leaves running the PI with services: SSH + FTP Server Proftpd**

 
````
root@PIROUTER:/lib# nconfig find br_netfilter 
./modules/4.14.98-v7+/kernel/net/bridge/br_netfilter.ko
./modules/4.14.98+/kernel/net/bridge/br_netfilter.ko
````

```` 
modprobe   br_netfilter
```` 

To Turn OFF the net (games...) but keep FTP: 
````
root@PIROUTER:/lib# iptables    -I FORWARD -m physdev --physdev-in wlan0 -j DROP 
````

__This command will block net forward:__
````
iptables    -I FORWARD -m physdev --physdev-in wlan0 -j DROP 
````


**2. Put Back the Access to Net**



__This command will allow net forward and all net connections from clients:__

````
root@PIROUTER:/lib#  iptables -F FORWARD   
````

**3. Crontab**

This can be automatic with crontab.



